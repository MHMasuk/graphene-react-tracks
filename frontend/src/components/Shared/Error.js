import React, { useState } from 'react';

// MUI STAFF 
import withStyles from '@material-ui/core/styles/withStyles';
import Button from '@material-ui/core/Button';
import SnackBar from '@material-ui/core/Snackbar';


const Error = ({ classes, error }) => {
    const [open, setOpen] = useState(true)

    return (
        <SnackBar
            open={open}
            className={classes.snackbar}
            message={error.message}
            action={
                <Button onClick={() => setOpen(false)} color="secondary" size="small">
                    Close
                </Button>
            }
        />
    )
}

const styles = theme => ({
    snackbar: {
        margin: theme.spacing(1)
    }
})

export default withStyles(styles)(Error);