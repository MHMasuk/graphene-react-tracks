import React from 'react';

import withStyles from '@material-ui/core/styles/withStyles';

const TrackList = ({ tracks }) => (
    <div>
        {tracks.map(track => (
            <p>{track.id}</p>
        ))
        }
    </div>
)

export default TrackList;