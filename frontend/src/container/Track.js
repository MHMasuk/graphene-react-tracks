import React from 'react';

// APOLLO CLIENT
import { Query } from 'react-apollo';
import { gql } from 'apollo-boost';

// MUI STAFF
import withStyles from "@material-ui/core/styles/withStyles";

// COMPONENTS
import Loading from '../components/Shared/Loading';
import Error from "../components/Shared/Error";
import TrackList from '../components/Track/TrackList';

const Track = ({ classes }) => {
    return (
        <div className={classes.container}>
            <Query query={GET_TRACKS_QUERY}>
                {({ data, loading, error }) => {
                    if (loading) return <Loading />;
                    if (error) return <Error error={error} />;
                    // const tracks = searchResults.

                    return <TrackList tracks={ data.tracks }/>
                }}
            </Query>

        </div>
    )
}

export const GET_TRACKS_QUERY = gql`
    query getTrackQuery {
        tracks {
            id
            title
            description
            url
            likes {
                id
            }
            postedBy {
                id
                username
            }
        }
    }
`;

const styles = theme => ({
    container: {
        margin: "0 auto",
        maxWidth: 960,
        padding: theme.spacing(2)
    }
});

export default withStyles(styles)(Track);