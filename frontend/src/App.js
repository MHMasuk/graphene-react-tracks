import React from 'react';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

// APOLLO CLINT
import { Query } from 'react-apollo';
import { gql } from 'apollo-boost'

// COMPONENTS
import withRoot from './withRoot';
import Track from './container/Track';
import Profile from './container/Profile';
import Header from './components/Shared/Header';
import Loading from './components/Shared/Loading';
import Error from './components/Shared/Error';

const App = () => (
  <Query query={ME_QUERY}>
    {({ data, loading, error }) => {
      if (loading) return <Loading />
      if (error) return <Error />
      const currentUser = data.me;

      return (
        <>
          <Router>
            <Header currentUser={currentUser}/>
            <Switch>
              <Route exact path="/" component={Track} />
              <Route path="/profile/:id" component={Profile} />
            </Switch>
          </Router>
        </>
      )
    }}
  </Query>
)

// const GET_TRACKS_QUERY = gql`
// {
//   tracks {
//     id
//     title
//     description
//     url
//   }
// }`

const ME_QUERY = gql`
{
  me {
    id
    username
    email
  }
}`

export default withRoot(App);
